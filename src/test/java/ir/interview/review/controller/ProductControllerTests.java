package ir.interview.review.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import ir.interview.review.model.Product;
import ir.interview.review.model.enumeration.ReviewType;
import ir.interview.review.repository.ProductRepository;
import ir.interview.review.service.dto.ProductDto;
import ir.interview.review.service.mapper.ProductMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@AutoConfigureMockMvc
@SpringBootTest
public class ProductControllerTests {

    private static final ObjectMapper mapper = createObjectMapper();
    public static final MediaType APPLICATION_JSON = MediaType.APPLICATION_JSON;
    @Autowired
    private MockMvc restPollMockMvc;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    private static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        return mapper.writeValueAsBytes(object);
    }

    private ProductDto createProductDto() {
        return ProductDto.builder()
                .productId(1L)
                .commentType(ReviewType.PUBLIC)
                .voteType(ReviewType.PUBLIC)
                .active(true)
                .build();
    }

    private Product createProduct1() {
        Product product = new Product();
        product.setProductId(1L);
        product.setActive(false);
        product.setVoteType(ReviewType.PUBLIC);
        product.setCommentType(ReviewType.PUBLIC);
        return product;
    }

    private Product createProduct2() {
        Product product = new Product();
        product.setProductId(2L);
        product.setActive(true);
        product.setVoteType(ReviewType.PUBLIC);
        product.setCommentType(ReviewType.PUBLIC);
        return product;
    }

    @Test
    public void createProductsCreatedTest() throws Exception {
        ProductDto productDto = createProductDto();
        restPollMockMvc.perform(post("/api/v1/products")
                        .contentType(APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(productDto)))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateProductsCreatedTest() throws Exception {
        Product product = productRepository.save(createProduct1());
        product.setActive(true);
        restPollMockMvc.perform(put("/api/v1/products")
                        .contentType(APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(productMapper.productToProductDto(product))))
                .andExpect(status().isOk());

        Optional<Product> productOptional = productRepository.findById(product.getProductId());
        assertThat(productOptional.get().getActive()).isEqualTo(true);
    }

    @Test
    public void getActiveProductsTest() throws Exception {
        productRepository.save(createProduct1());
        productRepository.save(createProduct2());
        restPollMockMvc.perform(get("/api/v1/activeProducts"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].productId").value(hasItem(2)))
                .andExpect(jsonPath("$.[*].averageVotes").value(hasItem(0.0)))
                .andExpect(jsonPath("$.[*].numberOfComments").value(hasItem(0)));
    }


}
