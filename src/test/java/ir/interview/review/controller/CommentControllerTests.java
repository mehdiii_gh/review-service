package ir.interview.review.controller;

import ir.interview.review.model.Comment;
import ir.interview.review.model.Product;
import ir.interview.review.model.enumeration.ConfirmType;
import ir.interview.review.model.enumeration.ReviewType;
import ir.interview.review.repository.CommentRepository;
import ir.interview.review.service.ProductService;
import ir.interview.review.service.dto.ProductDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
public class CommentControllerTests {

    @Autowired
    private MockMvc restPollMockMvc;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ProductService productService;

    @BeforeEach
    public void createProducts() {
        ProductDto productDto1 = ProductDto.builder()
                .productId(1L)
                .commentType(ReviewType.PUBLIC)
                .voteType(ReviewType.NOT_ACTIVE)
                .active(true)
                .build();
        productService.save(productDto1);

        ProductDto productDto2 = ProductDto.builder()
                .productId(3L)
                .commentType(ReviewType.NOT_ACTIVE)
                .voteType(ReviewType.NOT_ACTIVE)
                .active(true)
                .build();
        productService.save(productDto2);
    }

    @Test
    public void createCommentsCreatedTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/comments")
                        .param("productId", "1")
                        .param("comment", "nice product!"))
                .andExpect(status().isCreated());
    }

    @Test
    public void createCommentsInvalidProductTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/comments")
                        .param("productId", "0")
                        .param("comment", "nice product!"))
                .andExpect(status().isBadRequest())
                .andExpect(header().stringValues("X-review-service-alert", "Invalid productId"))
                .andExpect(header().stringValues("X-review-service-params", "0"));
    }

    @Test
    public void createCommentsInvalidCommentTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/comments")
                        .param("productId", "1")
                        .param("comment", ""))
                .andExpect(status().isBadRequest())
                .andExpect(header().stringValues("X-review-service-alert", "Invalid comment"))
                .andExpect(header().stringValues("X-review-service-params", ""));
    }

    @Test
    public void createCommentsNotFoundProductTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/comments")
                        .param("productId", "20")
                        .param("comment", "nice product!"))
                .andExpect(status().isNotFound())
                .andExpect(header().stringValues("X-review-service-alert", "Product not found"))
                .andExpect(header().stringValues("X-review-service-params", "20"));
    }

    @Test
    public void createCommentsNotActiveTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/comments")
                        .param("productId", "3")
                        .param("comment", "nice product!"))
                .andExpect(status().is(406))
                .andExpect(header().stringValues("X-review-service-alert", "Comment not active for this product"))
                .andExpect(header().stringValues("X-review-service-params", "3"));
    }

    private Comment addComment() {
        Optional<Product> product = productService.getOne(1L);
        Comment comment = new Comment();
        comment.setProduct(product.get());
        comment.setComment("nice product!");
        comment.setConfirmationType(ConfirmType.NOT_CHECKED);
        comment.setInsertDateTime(ZonedDateTime.now());
        return commentRepository.save(comment);
    }

    @Test
    public void getAllNotCheckedCommentsTest() throws Exception {
        addComment();
        restPollMockMvc.perform(get("/api/v1/notCheckedComments"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].productId").value(hasItem(1)))
                .andExpect(jsonPath("$.[*].comment").value(hasItem("nice product!")))
                .andExpect(jsonPath("$.[*].confirmationType").value(hasItem("NOT_CHECKED")));
    }

    @Test
    public void confirmCommentTest() throws Exception {
        Comment comment = addComment();
        restPollMockMvc.perform(get("/api/v1/confirmComment")
                        .param("commentId", comment.getCommentId().toString())
                        .param("confirm", "true"))
                .andExpect(status().isOk());
    }

    @Test
    public void confirmCommentNotFoundTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/confirmComment")
                        .param("commentId", "0")
                        .param("confirm", "true"))
                .andExpect(status().isNotFound())
                .andExpect(header().stringValues("X-review-service-alert", "Comment not found"))
                .andExpect(header().stringValues("X-review-service-params", "0"));
    }

}
