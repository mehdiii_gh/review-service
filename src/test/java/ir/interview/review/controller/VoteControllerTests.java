package ir.interview.review.controller;

import ir.interview.review.model.Product;
import ir.interview.review.model.Vote;
import ir.interview.review.model.enumeration.ConfirmType;
import ir.interview.review.model.enumeration.ReviewType;
import ir.interview.review.repository.VoteRepository;
import ir.interview.review.service.ProductService;
import ir.interview.review.service.dto.ProductDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
public class VoteControllerTests {

    @Autowired
    private MockMvc restPollMockMvc;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private ProductService productService;

    @BeforeEach
    public void createProducts() {
        ProductDto productDto1 = ProductDto.builder()
                .productId(1L)
                .commentType(ReviewType.NOT_ACTIVE)
                .voteType(ReviewType.PUBLIC)
                .active(true)
                .build();
        productService.save(productDto1);

        ProductDto productDto2 = ProductDto.builder()
                .productId(3L)
                .commentType(ReviewType.NOT_ACTIVE)
                .voteType(ReviewType.NOT_ACTIVE)
                .active(true)
                .build();
        productService.save(productDto2);
    }

    @Test
    public void createVotesCreatedTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/votes")
                        .param("productId", "1")
                        .param("vote", "5"))
                .andExpect(status().isCreated());
    }

    @Test
    public void createVotesInvalidProductTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/votes")
                        .param("productId", "0")
                        .param("vote", "5"))
                .andExpect(status().isBadRequest())
                .andExpect(header().stringValues("X-review-service-alert", "Invalid productId"))
                .andExpect(header().stringValues("X-review-service-params", "0"));
    }

    @Test
    public void createVotesInvalidVoteTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/votes")
                        .param("productId", "1")
                        .param("vote", "6"))
                .andExpect(status().isBadRequest())
                .andExpect(header().stringValues("X-review-service-alert", "Invalid vote (vote value should be between 0 and 5)"))
                .andExpect(header().stringValues("X-review-service-params", "6"));
    }

    @Test
    public void createVotesNotFoundProductTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/votes")
                        .param("productId", "10")
                        .param("vote", "3"))
                .andExpect(status().isNotFound())
                .andExpect(header().stringValues("X-review-service-alert", "Product not found"))
                .andExpect(header().stringValues("X-review-service-params", "10"));
    }

    @Test
    public void createVotesNotActiveTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/votes")
                        .param("productId", "3")
                        .param("vote", "5"))
                .andExpect(status().is(406))
                .andExpect(header().stringValues("X-review-service-alert", "Vote not active for this product"))
                .andExpect(header().stringValues("X-review-service-params", "3"));
    }

    private Vote addVote() {
        Optional<Product> product = productService.getOne(1L);
        Vote Vote = new Vote();
        Vote.setProduct(product.get());
        Vote.setVote(5);
        Vote.setConfirmationType(ConfirmType.NOT_CHECKED);
        Vote.setInsertDateTime(ZonedDateTime.now());
        return voteRepository.save(Vote);
    }

    @Test
    public void getAllNotCheckedVotesTest() throws Exception {
        addVote();
        restPollMockMvc.perform(get("/api/v1/notCheckedVotes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].productId").value(hasItem(1)))
                .andExpect(jsonPath("$.[*].vote").value(hasItem(5)))
                .andExpect(jsonPath("$.[*].confirmationType").value(hasItem("NOT_CHECKED")));
    }

    @Test
    public void confirmVoteTest() throws Exception {
        Vote vote = addVote();
        restPollMockMvc.perform(get("/api/v1/confirmVote")
                        .param("voteId", vote.getVoteId().toString())
                        .param("confirm", "true"))
                .andExpect(status().isOk());
    }

    @Test
    public void confirmVoteNotFoundTest() throws Exception {
        restPollMockMvc.perform(get("/api/v1/confirmVote")
                        .param("voteId", "0")
                        .param("confirm", "true"))
                .andExpect(status().isNotFound())
                .andExpect(header().stringValues("X-review-service-alert", "Vote not found"))
                .andExpect(header().stringValues("X-review-service-params", "0"));
    }

}
