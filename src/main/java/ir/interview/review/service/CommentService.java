package ir.interview.review.service;

import ir.interview.review.model.Product;
import ir.interview.review.service.dto.CommentDto;

import java.util.List;

public interface CommentService {

    /**
     * Save a comment.
     *
     * @param product the product that want to add comment.
     * @param comment the comment message.
     * @return the persisted entity.
     */
    CommentDto save(Product product, String comment);

    /**
     * Get comments that are not checked.
     *
     * @return List of not checked comments as CommentDto.
     */
    List<CommentDto> getNotChecked();

    /**
     * Set confirmation to a comment.
     *
     * @param commentId the comment id that you want to set confirmation.
     * @param confirm   the confirmation value.
     * @return the confirmed comment DTO.
     */
    CommentDto setConfirmation(Long commentId, Boolean confirm);
}
