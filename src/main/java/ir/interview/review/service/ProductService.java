package ir.interview.review.service;

import ir.interview.review.model.Product;
import ir.interview.review.service.dto.ProductDto;
import ir.interview.review.service.dto.ShowProductDto;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    /**
     * Save a product.
     *
     * @param productDto the DTO to save.
     * @return the persisted entity.
     */
    ProductDto save(ProductDto productDto);

    /**
     * Get active products.
     *
     * @return List of active products as ShowProductDTO.
     */
    List<ShowProductDto> getActives();

    /**
     * Get active products.
     *
     * @param productIds the list of product's ids.
     * @return List of active products as ShowProductDTO.
     */
    List<ShowProductDto> getByIds(List<Long> productIds);

    /**
     * Get all products.
     *
     * @return List of all products as ProductDto.
     */
    List<ProductDto> getAll();

    /**
     * Get one product.
     *
     * @param id the product id.
     * @return the product if existed.
     */
    Optional<Product> getOne(Long id);
}
