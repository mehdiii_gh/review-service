package ir.interview.review.service.impl;

import ir.interview.review.model.Product;
import ir.interview.review.model.Vote;
import ir.interview.review.model.enumeration.ConfirmType;
import ir.interview.review.repository.VoteRepository;
import ir.interview.review.service.VoteService;
import ir.interview.review.service.dto.VoteDto;
import ir.interview.review.service.mapper.VoteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class VoteServiceImpl implements VoteService {

    private final Logger log = LoggerFactory.getLogger(VoteServiceImpl.class);
    private final VoteRepository voteRepository;
    private final VoteMapper voteMapper;

    public VoteServiceImpl(VoteRepository voteRepository, VoteMapper voteMapper) {
        this.voteRepository = voteRepository;
        this.voteMapper = voteMapper;
    }

    @Override
    public VoteDto save(Product product, Integer vote) {
        return voteMapper.voteToVoteDto(voteRepository.save(voteMapper.newVoteToVote(product, vote)));
    }

    @Override
    public List<VoteDto> getNotChecked() {
        return voteRepository.findAllNotChecked().stream().map(voteMapper::voteToVoteDto).toList();
    }

    @Override
    public VoteDto setConfirmation(Long voteId, Boolean confirm) {
        Optional<Vote> voteOptional = voteRepository.findById(voteId);
        if (voteOptional.isPresent()) {
            ConfirmType type;
            if (confirm) type = ConfirmType.ACCEPT;
            else type = ConfirmType.REJECT;
            voteOptional.get().setConfirmationType(type);
            voteOptional.get().setConfirmationDateTime(ZonedDateTime.now());
            return voteMapper.voteToVoteDto(voteRepository.save(voteOptional.get()));
        } else {
            return null;
        }
    }
}
