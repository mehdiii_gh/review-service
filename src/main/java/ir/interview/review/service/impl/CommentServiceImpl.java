package ir.interview.review.service.impl;

import ir.interview.review.model.Comment;
import ir.interview.review.model.Product;
import ir.interview.review.model.enumeration.ConfirmType;
import ir.interview.review.repository.CommentRepository;
import ir.interview.review.service.CommentService;
import ir.interview.review.service.dto.CommentDto;
import ir.interview.review.service.mapper.CommentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    private final Logger log = LoggerFactory.getLogger(CommentServiceImpl.class);
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;

    public CommentServiceImpl(CommentRepository commentRepository, CommentMapper commentMapper) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
    }

    @Override
    public CommentDto save(Product product, String comment) {
        return commentMapper.commentToCommentDto(commentRepository.save(commentMapper.newCommentToComment(product, comment)));
    }

    @Override
    public List<CommentDto> getNotChecked() {
        return commentRepository.findAllNotChecked().stream().map(commentMapper::commentToCommentDto).toList();
    }

    @Override
    public CommentDto setConfirmation(Long commentId, Boolean confirm) {
        Optional<Comment> commentOptional = commentRepository.findById(commentId);
        if (commentOptional.isPresent()) {
            ConfirmType type;
            if (confirm) type = ConfirmType.ACCEPT;
            else type = ConfirmType.REJECT;
            commentOptional.get().setConfirmationType(type);
            commentOptional.get().setConfirmationDateTime(ZonedDateTime.now());
            return commentMapper.commentToCommentDto(commentRepository.save(commentOptional.get()));
        } else {
            return null;
        }
    }
}
