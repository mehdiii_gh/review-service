package ir.interview.review.service.impl;

import ir.interview.review.model.Comment;
import ir.interview.review.model.Product;
import ir.interview.review.model.Vote;
import ir.interview.review.model.enumeration.ConfirmType;
import ir.interview.review.repository.ProductRepository;
import ir.interview.review.service.ProductService;
import ir.interview.review.service.dto.ProductDto;
import ir.interview.review.service.dto.ShowProductDto;
import ir.interview.review.service.mapper.ProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

@Service
public class ProductServiceImpl implements ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public ProductDto save(ProductDto productDto) {
        Optional<Product> productOptional = productRepository.findById(productDto.getProductId());
        if (productOptional.isPresent()) {
            return productMapper.productToProductDto(productRepository.save(productMapper.productDtoToUpdateProduct(productOptional.get(), productDto)));
        } else {
            return productMapper.productToProductDto(productRepository.save(productMapper.productDtoToNewProduct(productDto)));
        }
    }

    @Override
    public List<ShowProductDto> getActives() {
        return productRepository.findAllActives().stream()
                .map(product -> productMapper.productToShowProductDto(
                        product,
                        calculateAverageVotes(product.getVotes()),
                        calculateNumberOfComments(product.getComments())))
                .toList();
    }

    @Override
    public List<ShowProductDto> getByIds(List<Long> productIds) {
        return productRepository.findByIds(productIds).stream()
                .map(product -> productMapper.productToShowProductDto(
                        product,
                        calculateAverageVotes(product.getVotes()),
                        calculateNumberOfComments(product.getComments())))
                .toList();
    }

    @Override
    public List<ProductDto> getAll() {
        return productRepository.findAll().stream().map(productMapper::productToProductDto).toList();
    }

    @Override
    public Optional<Product> getOne(Long id) {
        return productRepository.findById(id);
    }

    private Double calculateAverageVotes(List<Vote> votes) {
        OptionalDouble average = votes.stream().filter(vote -> vote.getConfirmationType() == ConfirmType.ACCEPT).mapToDouble(Vote::getVote).average();
        return average.isPresent() ? average.getAsDouble() : 0.0;
    }

    private Integer calculateNumberOfComments(List<Comment> comments) {
        return Math.toIntExact(comments.stream().filter(comment -> comment.getConfirmationType() == ConfirmType.ACCEPT).count());
    }
}
