package ir.interview.review.service;

import ir.interview.review.model.Product;
import ir.interview.review.service.dto.VoteDto;

import java.util.List;

public interface VoteService {

    /**
     * Save a vote.
     *
     * @param product the product that want to add vote.
     * @param vote    the vote number.
     * @return the persisted entity.
     */
    VoteDto save(Product product, Integer vote);

    /**
     * Get votes that are not checked.
     *
     * @return List of not checked votes as VoteDto.
     */
    List<VoteDto> getNotChecked();

    /**
     * Set confirmation to a vote.
     *
     * @param voteId  the vote id that you want to set confirmation.
     * @param confirm the confirmation value.
     * @return the confirmed comment DTO.
     */
    VoteDto setConfirmation(Long voteId, Boolean confirm);
}
