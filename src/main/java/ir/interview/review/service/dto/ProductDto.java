package ir.interview.review.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import ir.interview.review.model.enumeration.ReviewType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto {
    private Long productId;
    private Boolean active;
    private ReviewType commentType;
    private ReviewType voteType;
}
