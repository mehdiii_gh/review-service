package ir.interview.review.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import ir.interview.review.model.enumeration.ConfirmType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentDto {
    private Long commentId;
    private Long productId;
    private String comment;
    private ConfirmType confirmationType;
}
