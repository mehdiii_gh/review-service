package ir.interview.review.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import ir.interview.review.model.enumeration.ConfirmType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VoteDto {
    private Long voteId;
    private Long productId;
    private Integer vote;
    private ConfirmType confirmationType;
}
