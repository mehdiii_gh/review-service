package ir.interview.review.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ShowCommentDto {
    private String comment;
    private String insertDateTime;
}
