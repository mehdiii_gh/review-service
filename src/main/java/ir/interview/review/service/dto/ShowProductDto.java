package ir.interview.review.service.dto;

import ir.interview.review.model.enumeration.ReviewType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class ShowProductDto {
    private Long productId;
    private ReviewType commentType;
    private ReviewType voteType;
    private Double averageVotes;
    private Integer numberOfComments;
    private List<ShowCommentDto> comments;
}
