package ir.interview.review.service.mapper;

import ir.interview.review.model.Product;
import ir.interview.review.model.Vote;
import ir.interview.review.model.enumeration.ConfirmType;
import ir.interview.review.service.dto.VoteDto;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
public class VoteMapper {

    public Vote newVoteToVote(Product product, Integer voteValue) {
        Vote vote = new Vote();
        vote.setProduct(product);
        vote.setVote(voteValue);
        vote.setConfirmationType(ConfirmType.NOT_CHECKED);
        vote.setInsertDateTime(ZonedDateTime.now());
        return vote;
    }

    public VoteDto voteToVoteDto(Vote vote) {
        return VoteDto.builder()
                .voteId(vote.getVoteId())
                .productId(vote.getProduct().getProductId())
                .confirmationType(vote.getConfirmationType())
                .vote(vote.getVote())
                .build();
    }

}
