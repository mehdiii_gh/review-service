package ir.interview.review.service.mapper;

import ir.interview.review.model.Product;
import ir.interview.review.model.enumeration.ConfirmType;
import ir.interview.review.service.dto.ProductDto;
import ir.interview.review.service.dto.ShowCommentDto;
import ir.interview.review.service.dto.ShowProductDto;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;

@Service
public class ProductMapper {

    public ProductDto productToProductDto(Product product) {
        return ProductDto.builder()
                .productId(product.getProductId())
                .commentType(product.getCommentType())
                .voteType(product.getVoteType())
                .active(product.getActive())
                .build();
    }

    public ShowProductDto productToShowProductDto(Product product, Double votesAverage, Integer numberOfComments) {
        return ShowProductDto.builder()
                .productId(product.getProductId())
                .commentType(product.getCommentType())
                .voteType(product.getVoteType())
                .averageVotes(votesAverage)
                .numberOfComments(numberOfComments)
                .comments(product.getComments().stream()
                        .filter(comment -> comment.getConfirmationType() == ConfirmType.ACCEPT)
                        .map(temp -> ShowCommentDto.builder()
                                .comment(temp.getComment())
                                .insertDateTime(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(temp.getInsertDateTime()))
                                .build()).limit(3).toList())
                .build();
    }

    public Product productDtoToNewProduct(ProductDto productDto) {
        Product product = new Product();
        product.setProductId(productDto.getProductId());
        product.setActive(productDto.getActive());
        product.setCommentType(productDto.getCommentType());
        product.setVoteType(productDto.getVoteType());
        return product;
    }

    public Product productDtoToUpdateProduct(Product product, ProductDto productDto) {
        if (productDto.getActive() != null) product.setActive(productDto.getActive());
        if (productDto.getCommentType() != null) product.setCommentType(productDto.getCommentType());
        if (productDto.getVoteType() != null) product.setVoteType(productDto.getVoteType());
        return product;
    }

}
