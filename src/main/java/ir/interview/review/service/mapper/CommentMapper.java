package ir.interview.review.service.mapper;

import ir.interview.review.model.Comment;
import ir.interview.review.model.Product;
import ir.interview.review.model.enumeration.ConfirmType;
import ir.interview.review.service.dto.CommentDto;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
public class CommentMapper {

    public Comment newCommentToComment(Product product, String commentMessage) {
        Comment comment = new Comment();
        comment.setProduct(product);
        comment.setComment(commentMessage);
        comment.setConfirmationType(ConfirmType.NOT_CHECKED);
        comment.setInsertDateTime(ZonedDateTime.now());
        return comment;
    }

    public CommentDto commentToCommentDto(Comment comment) {
        return CommentDto.builder()
                .commentId(comment.getCommentId())
                .comment(comment.getComment())
                .productId(comment.getProduct().getProductId())
                .confirmationType(comment.getConfirmationType())
                .build();
    }
}
