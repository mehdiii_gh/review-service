package ir.interview.review.repository;

import ir.interview.review.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    @Query("select cm from Comment cm where cm.confirmationType = ir.interview.review.model.enumeration.ConfirmType.NOT_CHECKED order by cm.insertDateTime desc")
    List<Comment> findAllNotChecked();
}
