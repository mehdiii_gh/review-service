package ir.interview.review.repository;

import ir.interview.review.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("select pr from Product pr where pr.active = true")
    List<Product> findAllActives();

    @Query("select pr from Product pr where pr.productId in (:productIds) and pr.active = true")
    List<Product> findByIds(@Param("productIds") List<Long> productIds);
}
