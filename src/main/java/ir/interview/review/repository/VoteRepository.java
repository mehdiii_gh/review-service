package ir.interview.review.repository;

import ir.interview.review.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VoteRepository extends JpaRepository<Vote, Long>{
    @Query("select v from Vote v where v.confirmationType = ir.interview.review.model.enumeration.ConfirmType.NOT_CHECKED order by v.insertDateTime desc")
    List<Vote> findAllNotChecked();
}
