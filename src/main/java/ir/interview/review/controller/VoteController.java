package ir.interview.review.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import ir.interview.review.model.Product;
import ir.interview.review.model.enumeration.ReviewType;
import ir.interview.review.service.ProductService;
import ir.interview.review.service.VoteService;
import ir.interview.review.service.dto.VoteDto;
import ir.interview.review.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class VoteController {

    private final VoteService voteService;
    private final ProductService productService;

    public VoteController(VoteService voteService, ProductService productService) {
        this.voteService = voteService;
        this.productService = productService;
    }

    private static final String ENTITY_NAME = "vote";

    @Operation(summary = "Create a vote for a product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Vote created"),
            @ApiResponse(responseCode = "400", description = "Invalid product id or Invalid vote value"),
            @ApiResponse(responseCode = "404", description = "Product not found"),
            @ApiResponse(responseCode = "406", description = "Vote not acceptable because vote not active for this product")})
    @GetMapping("/votes")
    public ResponseEntity<VoteDto> createVotes(@Parameter(description = "Product ID")
                                               @RequestParam(value = "productId") Long productId,
                                               @Parameter(description = "Vote value between 0 and 5")
                                               @RequestParam(value = "vote") Integer vote) throws URISyntaxException {
        if (productId == null || productId <= 0) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createAlert("Invalid productId", productId)).build();
        }
        if (vote == null || vote < 0 || vote > 5) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createAlert("Invalid vote (vote value should be between 0 and 5)", vote)).build();
        }
        Optional<Product> productOptional = productService.getOne(productId);
        if (productOptional.isEmpty()) {
            return ResponseEntity.notFound().headers(HeaderUtil.createAlert("Product not found", productId)).build();
        }
        if (productOptional.get().getVoteType() == ReviewType.NOT_ACTIVE) {
            return ResponseEntity.status(406).headers(HeaderUtil.createAlert("Vote not active for this product", productId)).build();
        }
        VoteDto result = voteService.save(productOptional.get(), vote);
        return ResponseEntity.created(new URI("/api/votes/" + result.getVoteId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getVoteId().toString()))
                .body(result);
    }

    @Operation(summary = "Get not checked votes by system admin")
    @GetMapping("/notCheckedVotes")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "List of not checked votes")})
    public ResponseEntity<List<VoteDto>> getAllNotCheckedVotes() {
        List<VoteDto> result = voteService.getNotChecked();
        return ResponseEntity.ok().headers(HeaderUtil.createTotalCountHeader(result.size())).body(result);
    }

    @Operation(summary = "Confirm vote with vote id by system admin")
    @GetMapping("/confirmVote")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Vote that is accept or reject"),
            @ApiResponse(responseCode = "404", description = "Vote not found")
    })
    public ResponseEntity<VoteDto> confirmVote(@RequestParam(value = "voteId") Long voteId,
                                               @RequestParam(value = "confirm") Boolean confirm) {
        VoteDto result = voteService.setConfirmation(voteId, confirm);
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createAlert("Vote not found", voteId)).build();
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getVoteId().toString()))
                .body(result);
    }
}
