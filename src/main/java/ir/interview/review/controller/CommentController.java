package ir.interview.review.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import ir.interview.review.model.Product;
import ir.interview.review.model.enumeration.ReviewType;
import ir.interview.review.service.CommentService;
import ir.interview.review.service.ProductService;
import ir.interview.review.service.dto.CommentDto;
import ir.interview.review.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class CommentController {

    private static final String ENTITY_NAME = "comment";
    private final CommentService commentService;

    private final ProductService productService;

    public CommentController(CommentService commentService, ProductService productService) {
        this.commentService = commentService;
        this.productService = productService;
    }

    @Operation(summary = "Create a comment for a product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Comment created"),
            @ApiResponse(responseCode = "400", description = "Invalid product id or Empty comment"),
            @ApiResponse(responseCode = "404", description = "Product not found"),
            @ApiResponse(responseCode = "406", description = "Comment not acceptable because comment not active for this product")})
    @GetMapping("/comments")
    public ResponseEntity<CommentDto> createComments(@Parameter(description = "Product ID")
                                                     @RequestParam(value = "productId") Long productId,
                                                     @Parameter(description = "Comment message")
                                                     @RequestParam(value = "comment") String comment) throws URISyntaxException {
        if (productId == null || productId <= 0) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createAlert("Invalid productId", productId)).build();
        }
        if (comment == null || comment.isEmpty()) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createAlert("Invalid comment", comment)).build();
        }
        Optional<Product> productOptional = productService.getOne(productId);
        if (productOptional.isEmpty()) {
            return ResponseEntity.notFound().headers(HeaderUtil.createAlert("Product not found", productId)).build();
        }
        if (productOptional.get().getCommentType() == ReviewType.NOT_ACTIVE) {
            return ResponseEntity.status(406).headers(HeaderUtil.createAlert("Comment not active for this product", productId)).build();
        }
        CommentDto result = commentService.save(productOptional.get(), comment);
        return ResponseEntity.created(new URI("/api/comments/" + result.getCommentId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getCommentId().toString()))
                .body(result);
    }

    @Operation(summary = "Get not checked comments by system admin")
    @GetMapping("/notCheckedComments")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "List of not checked comments")})
    public ResponseEntity<List<CommentDto>> getAllNotCheckedComments() {
        List<CommentDto> result = commentService.getNotChecked();
        return ResponseEntity.ok().headers(HeaderUtil.createTotalCountHeader(result.size())).body(result);
    }

    @Operation(summary = "Confirm comment with comment id by system admin")
    @GetMapping("/confirmComment")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Comment that is accept or reject"),
            @ApiResponse(responseCode = "404", description = "Comment not found")
    })
    public ResponseEntity<CommentDto> confirmComment(@Parameter(description = "Comment ID")
                                                     @RequestParam(value = "commentId") Long commentId,
                                                     @Parameter(description = "Confirm value")
                                                     @RequestParam(value = "confirm") Boolean confirm) {
        CommentDto result = commentService.setConfirmation(commentId, confirm);
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createAlert("Comment not found", commentId)).build();
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getCommentId().toString()))
                .body(result);
    }

}
