package ir.interview.review.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import ir.interview.review.model.Product;
import ir.interview.review.service.ProductService;
import ir.interview.review.service.dto.ProductDto;
import ir.interview.review.service.dto.ShowProductDto;
import ir.interview.review.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class ProductController {

    private static final String ENTITY_NAME = "product";
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Operation(summary = "Create a product by system admin")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Product created"),
            @ApiResponse(responseCode = "400", description = "Product already exist")
    })
    @PostMapping("/products")
    public ResponseEntity<ProductDto> createProduct(@RequestBody ProductDto productDto) throws URISyntaxException {
        Optional<Product> productOptional = productService.getOne(productDto.getProductId());
        if (productOptional.isPresent()) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createAlert("Product already exist", productDto.getProductId())).build();
        }
        ProductDto result = productService.save(productDto);
        return ResponseEntity.created(new URI("/api/products/" + result.getProductId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getProductId().toString()))
                .body(result);
    }

    @Operation(summary = "Update a product by system admin")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product created"),
            @ApiResponse(responseCode = "400", description = "Invalid product id")
    })
    @PutMapping("/products")
    public ResponseEntity<ProductDto> updateProduct(@RequestBody ProductDto productDto) {
        if (productDto.getProductId() == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createAlert("Invalid product id", productDto.getProductId())).build();
        }
        ProductDto result = productService.save(productDto);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getProductId().toString()))
                .body(result);
    }

    @Operation(summary = "Get list of input products")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "List of input products")})
    @GetMapping("/products")
    public ResponseEntity<List<ShowProductDto>> getProducts(@Parameter(description = "List of product ids")
                                                            @RequestParam(value = "productList") List<Long> productIds) {
        List<ShowProductDto> result = productService.getByIds(productIds);
        return ResponseEntity.ok().headers(HeaderUtil.createTotalCountHeader(result.size())).body(result);
    }

    @Operation(summary = "Get list all products by system admin")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "List of all products")})
    @GetMapping("/allProducts")
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        List<ProductDto> result = productService.getAll();
        return ResponseEntity.ok().headers(HeaderUtil.createTotalCountHeader(result.size())).body(result);
    }

    @Operation(summary = "Get list of all active products")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "List of all active products")})
    @GetMapping("/activeProducts")
    public ResponseEntity<List<ShowProductDto>> getActiveProducts() {
        List<ShowProductDto> result = productService.getActives();
        return ResponseEntity.ok().headers(HeaderUtil.createTotalCountHeader(result.size())).body(result);
    }
}
