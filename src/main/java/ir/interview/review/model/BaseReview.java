package ir.interview.review.model;

import ir.interview.review.model.enumeration.ConfirmType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.ZonedDateTime;

@Getter
@Setter
@MappedSuperclass
public class BaseReview {

    @Column(name = "confirmation_type", nullable = false)
    private ConfirmType confirmationType;

    @Column(name = "insert_date_time", nullable = false)
    private ZonedDateTime insertDateTime;

    @Column(name = "confirmation_date_time")
    private ZonedDateTime confirmationDateTime;
}
