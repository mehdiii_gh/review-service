package ir.interview.review.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "vote")
@Getter
@Setter
@NoArgsConstructor
public class Vote extends BaseReview {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long voteId;

    @Column(name = "vote", nullable = false)
    private Integer vote;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    private Product product;
}
