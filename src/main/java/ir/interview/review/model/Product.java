package ir.interview.review.model;

import ir.interview.review.model.enumeration.ReviewType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product")
@Getter
@Setter
@NoArgsConstructor
public class Product {
    @Id
    private Long productId;

    @Column(name = "active", nullable = false)
    private Boolean active;

    @Column(name = "comment_type", nullable = false)
    private ReviewType commentType;

    @Column(name = "vote_type", nullable = false)
    private ReviewType voteType;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<Vote> votes = new ArrayList<>();

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    @OrderBy("insertDateTime DESC ")
    private List<Comment> comments = new ArrayList<>();

}
