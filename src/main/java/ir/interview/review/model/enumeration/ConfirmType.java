package ir.interview.review.model.enumeration;

public enum ConfirmType {
    NOT_CHECKED,
    ACCEPT,
    REJECT
}
