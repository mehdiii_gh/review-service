package ir.interview.review.model.enumeration;

public enum ReviewType {
    NOT_ACTIVE,
    PUBLIC,
    BUYER
}
